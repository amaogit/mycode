#!/usr/bin/python3

import requests

NASAAPI = "https://api.nasa.gov/planetary/apod?"
SDATE= input("Enter a date in YYYY-MM-DD format for query: ")
#COUNT="&count=5"
#RDATE="&start_date=2022-10-13&end_date=2022-10-17"

# this function grabs our credentials
def returncreds():
    ## first I want to grab my credentials
    with open("/home/student/nasa.creds", "r") as mycreds:
        nasacreds = mycreds.read()
    ## remove any newline characters from the api_key
    nasacreds = "api_key=" + nasacreds.strip("\n")
    return nasacreds

# this is our main function
def main():
    ## first grab credentials
    nasacreds = returncreds()

    ## make a call to NASAAPI with our key
    #apodresp = requests.get(NASAAPI + nasacreds)
    #sdate = requests.get(NASAAPI + nasacreds + SDATE )
    #rdate = requests.get(NASAAPI + nasacreds + RDATE )
    #count = requests.get(NASAAPI + nasacreds + COUNT )
    ## Create URL
    URL = f"{NASAAPI}{nasacreds}&date={SDATE}"
    print(URL)
    ## Request to get response
    resp= requests.get(URL).json()
    print(resp)

    # print 5 dates
    ## print(count.json())

    # print date range
    ## print(rdate.json())


if __name__ == "__main__":
    main()

