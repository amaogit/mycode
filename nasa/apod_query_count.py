#!/usr/bin/python3

import requests

NASAAPI = "https://api.nasa.gov/planetary/apod?"
COUNT= input("Enter how many random queries: ")

# this function grabs our credentials
def returncreds():
    ## first I want to grab my credentials
    with open("/home/student/nasa.creds", "r") as mycreds:
        nasacreds = mycreds.read()
    ## remove any newline characters from the api_key
    nasacreds = "api_key=" + nasacreds.strip("\n")
    return nasacreds

# this is our main function
def main():
    ## first grab credentials
    nasacreds = returncreds()

    ## Create URL for random query
    URL = f"{NASAAPI}{nasacreds}&count={COUNT}"
    print(URL)
    ## Request to get response
    resp= requests.get(URL).json()
    print(resp)

if __name__ == "__main__":
    main()

