#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests (get it?)
import requests

#URL = "https://swapi.dev/luke/force"      # Comment out this line
URL= "https://swapi.dev/api/people/13/"     # Uncomment this line

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp= requests.get(URL)

    # Check resp method, etc
    # print(dir(resp))
    # print out status_code
    #print(resp.status_code)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        chewbacca= resp.json()
        pprint(chewbacca)
        URL2=chewbacca['films'][0]
        resp2=requests.get(URL2)
        film=resp2.json()
        #print(film)
        resp3=requests.get(film['starships'][-1])
        name=resp3.json()
        print(name)
        #print("He first appeared in the movie", film['title'], "and could be found flying around in his", name['name'],)
        print(f"He first appeared in the movie {film['title']}, and could be found flying around in his {name['name']}")

    else:
        print("That is not a valid URL.")

if __name__ == "__main__":
    main()

