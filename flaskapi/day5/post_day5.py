import requests

# the URL you wish to post to
#url = 'https://alta3.com/login'
url= 'https://aux1-2700e185-c3c8-4a5e-afce-0861f61e583b.live.alta3.com/login'

# the data you wish to post
ye_olde_dict = {'hello': 'world'}

# post the data to the URL, capture the response as "x"
x = requests.post(url, json = ye_olde_dict)

# print the text response you get back to confirm all is well
print(x.text)
