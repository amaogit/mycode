#!/usr/bin/python3

# use a single line for each import
from flask import Flask
from flask import redirect
from flask import url_for
from flask import request
from flask import render_template

app = Flask(__name__)
## This is where we want to redirect users to
@app.route("/success/<name>")
def success(name):
    return f"Welcome {name}\n"
# This is a landing point for users (a start)
@app.route("/") # user can land at "/"
@app.route("/start") # or user can land at "/start"
def start():
    return render_template("trivia.html")
    #return render_template("postmaker.html") # look for templates/postmaker.html
@app.route("/login", methods = ["POST", "GET"])
def login():
    # POST would likely come from a user interacting with postmaker.html
    if request.form.get("nm")
        answer = request.form.get("nm")
        if answer == "42":
            return redirect(url_for("success", name = answer))
if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224)

