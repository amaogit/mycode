#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests (get it?)
import requests

URL= "https://swapi.dev/api/people/20/"     # Uncomment this line

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp= requests.get(URL)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary



        vader= resp.json()
        print(f"{vader['name']} was born in the year {vader['birth_year']}. His eyes are now {vader['eye_color']} and his hair color is {'hair_color'}.")

        firstmovie= vader["films"][0]
        # firstmovie= 'https://swapi.dev/api/films/1/'
        # sending a GET to the url, converting the json, and grabbing the value of "title"
        movie_title= requests.get(firstmovie).json()["title"]
       
        # is there anything in the starships list
        if vader["starships"]:
            firstship= vader["starships"][0]
            # firstmovie= 'https://swapi.dev/api/starships/13/'
            # sending a GET to the url, converting the json, and grabbing the value of "title"
            ship_name= requests.get(firstship).json()["name"]
        else:
            ship_name= "his buddies' starships instead."

        print(f"He first appeared in the movie {movie_title} and could be found flying around in his {ship_name}.")


    else:
        print("That is not a valid URL.")

if __name__ == "__main__":
    main()

