#!/usr/bin/python3

import requests

URL= "http://api.open-notify.org/iss-now.json"

def main():
    resp= requests.get(URL)
    print(resp.json())
    resp=resp.json()
    print(resp)

    lon=resp['iss_position']['longitude']
    lat=resp['iss_position']['latitude']
    print(f"CURRENT LOCATION OF THE ISS: ")
    print(f"Lon: {lon}")
    print(f"Lat: {lat}")
if __name__ == "__main__":
    main ()
